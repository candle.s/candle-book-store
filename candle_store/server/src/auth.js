const { sign } = require('jsonwebtoken')

exports.createaAccessToken = user => {
  return sign({ 
    userId: user._id
  }, process.env.ACCESS_SECRET, { expiresIn: '15m' })
}

exports.createRefreshToken = user => {
  return sign({
    userId: user._id,
    tokenVersion: user.tokenVersion
  }, process.env.REFRESH_SECRET, { expiresIn: '7d' })
}