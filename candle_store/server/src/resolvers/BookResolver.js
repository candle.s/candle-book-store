const path = require('path')
const fs = require('fs')
const Book = require('../models/books')
const Author = require('../models/authors')
const { uploadWebp } = require('../utils/upload')

module.exports = {
  Query: {
    featureBooks: async () => {
      const result = await Book.find({}).sort({'createdAt': -1}).limit(6).populate('category', 'name').populate('author', 'name')
      if(result) {
        return {
          success: true,
          message: 'ok',
          result
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: []
        }
      }
    },
    books: async (_, { page: pagePaginate }) => {
      const page = parseInt(pagePaginate) || 0
      const limit = 6
      const result = await Book.find({})
        .sort({'createdAt': -1})
        .skip(page * limit)
        .limit(limit)
        .populate('category', 'name')
        .populate('author', 'name')
      const resultBooks = await Book.find({})
      if(result) {
        return {
          success: true,
          message: 'ok',
          result,
          totalPage: Math.ceil(resultBooks.length / limit)
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: [],
          totalPage: 0
        }
      }
    },
    book: async (_, { slug }) => {
      const result = await Book.find({'slug': slug}).populate('category', 'name').populate('author', 'name')
      if(result.length > 0) {
        return {
          success: true,
          message: 'ok',
          result
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: []
        }
      }
    },
  },
  Mutation: {
    createBook: async (_, { name, detail, category, author, cover }, { req }) => {
      const slug = name.trim().toLowerCase()
        .replace(/'/g, '_')
        .replace(/"/g, '_').replace(/ /g, '_').replace(/&/g, '_')
      const book = await Book.findOne({'slug': slug})
      const resultUpload = await uploadWebp(cover)

      if(!book) {
        const createBook = await Book.create({
          name,
          detail,
          category,
          author,
          cover: resultUpload,
          slug
        })
        if(createBook) {
          const resultAuthor = await Author.find({ books: createBook._id })
          if(resultAuthor.length === 0) {
            await Author.update({'_id': author}, { $push: { books: createBook._id } })
          }
          const book = await Book.findById(createBook._id).populate('category', 'name').populate('author', 'name')
          return {
            success: true,
            message: 'book created',
            result: [book],
          }
        } else {
          return {
            success: false,
            message: 'not found',
            result: [],
          }
        }
      } else {
        return {
          success: false,
          message: 'slug is already',
          result: [],
        }
      }
    },
    updateBook: async (_, { name, detail, category, author, cover, slug }) => {
      const book = await Book.findOne({'slug': slug})
      const updateValue = {}
      const { filename } = await cover
      if(filename) {
        updateValue.cover = await uploadWebp(cover)
        fs.unlink(path.join(__dirname, '../uploads', `${book.cover}.jpg`), (err) => {
          if(err) console.log(err)
        })
        fs.unlink(path.join(__dirname, '../uploads', `${book.cover}.webp`), (err) => {
          if(err) console.log(err)
        })
      } else {
        updateValue.cover = book.cover
      }
      if(name) updateValue.name = name
      if(name) {
        const slug = name.trim().toLowerCase()
          .replace(/'/g, '_')
          .replace(/"/g, '_').replace(/ /g, '_').replace(/&/g, '_')
        updateValue.slug = slug
      }
      if(detail) updateValue.detail = detail
      if(category) updateValue.category = category
      if(author) updateValue.author = author
      
      const resultAuthor = await Author.find({ books: book._id })
      if(resultAuthor.length === 0) {
        await Author.update({'_id': author}, { $push: { books: book._id } })
      }

      const result = await Book.findOneAndUpdate(
        { 'slug': slug }, {$set: updateValue}, 
        { new: true })
        .populate('category', 'name')
        .populate('author', 'name')
      if(result) {
        return {
          success: true,
          message: 'book updated',
          result: [result]
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: []
        }
      }
    },
    deleteBook: async (_, { id }) => {
      const book = await Book.findById(id)
      fs.unlink(path.join(__dirname, '../uploads', `${book.cover}.jpg`), (err) => {
        if(err) console.log(err)
        fs.unlink(path.join(__dirname, '../uploads', `${book.cover}.webp`), (err) => {
          if(err) console.log(err)
        })
      })
      await Author.update({'_id': book.author}, { $pull: { books: book._id } })
      const result = await Book.findByIdAndRemove(id)
      if(result) {
        return {
          success: true,
          message: 'book deleted',
          result: []
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: []
        }
      }
    }
  }
}