const Category = require('../models/category')

module.exports = {
  Query: {
    categories: async () => {
      const result = await Category.find({}).sort({ 'name': 1 })
      return {
        success: true,
        message: 'ok',
        result: result
      }
    }
  },
  Mutation: {
    category: async (_, { id }) => {
      const result = await Category.findOne('_id', id)
      if(result) {
        return {
          success: true,
          message: 'ok',
          result: [result]
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: []
        }
      }
    },
    createCategory: async (_, { name }) => {
      const result = await Category.create({ name })
      if(result) {
        return {
          success: true,
          message: 'category created',
          result: [result]
        }
      } else {
        return {
          success: false,
          message: 'something wrong create not create',
          result: []
        }
      }
    }
  }
}