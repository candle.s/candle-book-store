const fs = require('fs')
const path = require('path')
const Author = require('../models/authors')
const { uploadWebp } = require('../utils/upload')

module.exports = {
  Query: {
    authors: async (_, {page: pagePaginate}) => {
      const page = parseInt(pagePaginate) || 0
      const limit = 6
      const result = await Author.find({})
        .sort({ 'name': -1 })
        .skip(page * limit)
        .limit(limit)
      if(result.length > 0) {
        return {
          success: true,
          message: 'ok',
          result,
          totalPage: Math.ceil(result.length / limit)
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: [],
          totalPage: 0
        }
      }
    },
    author: async (_, { id }) => {
      if(id) {
        const result = await Author.findById(id).populate('books')
        if(result) {
          return {
            success: true,
            message: 'ok',
            result: [result]
          }
        } else {
          return {
            success: false,
            message: 'not found',
            result: []
          }
        }
      } else {
        return {
          success: false,
          message: 'not found',
          result: []
        }
      }
    }
  },
  Mutation: {
    createAuthor: async (_, { name, bio, cover }) => {
      const resultUpload = await uploadWebp(cover, 'authors')
      const result = await Author.create({ name, bio, cover: resultUpload })
      if(result) {
        return {
          success: true,
          message: 'author created',
          result: [result]
        }
      } else {
        return {
          success: false,
          message: 'author failed',
          result: []
        }
      }
    },
    updateAuthor: async (_, { id, name, bio, cover }) => {
      const author = await Author.findOne({'_id': id})
      const updateValue = {}
      const { filename } = await cover
      if(filename) {
        updateValue.cover = await uploadWebp(cover, 'authors')
        fs.unlink(path.join(__dirname, '../uploads/authors', `${author.cover}.jpg`), (err) => {
        if(err) console.log(err)
        })
        fs.unlink(path.join(__dirname, '../uploads/authors', `${author.cover}.webp`), (err) => {
          if(err) console.log(err)
        })
      } else {
        updateValue.cover = author.cover
      }
      if(name) updateValue.name = name
      if(bio) updateValue.bio = bio
      const result = await Author.findOneAndUpdate({'_id': id}, {$set: updateValue}, { new: true })
      if(result) {
        return {
          success: true,
          message: 'author updated',
          result: [result]
        }
      } else {
        return {
          success: false,
          message: 'author failed',
          result: []
        }
      }
    }
  }
}