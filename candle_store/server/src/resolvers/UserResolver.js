const Users = require('../models/users')
const bcrypt = require('bcrypt')
const { createRefreshToken, createaAccessToken } = require('../auth')
const { verify } = require('jsonwebtoken')

module.exports = {
  Query: {
    users: async () => {
      const result = await Users.find({}, { '_id': 1, 'name': 1, 'age': 1 }).sort({ 'name': 1 })
      if(result) {
        return {
          success: true,
          message: 'ok',
          result
        }
      } else {
        return {
          success: false,
          message: 'user is not defined',
          result: []
        }
      }
    },
    me: async (_, __, { req }) => {
      const authorization = req.headers['authorization']
      if(!authorization) {
        return {
          id: '',
          email: ''
        }
      }

      try {
        const token = authorization.split(' ')[1]
        const payload = verify(token, process.env.ACCESS_SECRET)
        const user = await Users.findOne({'_id': payload.userId})
        return {
          id: user._id,
          email: user.email
        }
      } catch(err) {
        return {
          id: '',
          email: ''
        }
      }
    }
  },
  Mutation: {
    revokeRefreshTokenForUser: async (_, { id }) => {
      await Users.findOneAndUpdate({'_id': id}, {$inc: { tokenVersion: 1 }})
      return true
    },
    checkAuth: async (_, __, { payload }) => {
      if(payload) {
        const resultUser = await Users.findOne({ '_id': payload.userId })
        if(resultUser) {
          return {
            success: true
          }
        } else {
          return {
            success: false
          }
        }
      } else {
        return {
          success: false
        }
      }
    },
    user: async (_, { id }) => {
      const resultUser = await Users.findOne({ '_id': id })
      if(resultUser) {
        return {
          success: true,
          message: 'ok',
          result: [resultUser]
        }
      } else {
        return {
          success: false,
          message: 'user is not defined',
          result: []
        }
      }
    },
    login: async (_, { email, password }, { req, res }) => {
      if(!email || !password) {
        return {
          accessToken: '',
          user: ''
        }
      } else {
        const user = await Users.findOne({ email })
        if(!user) {
          return {
            accessToken: '',
            user: ''
          }
        } else {
          const isHash = await bcrypt.compare(password, user.password)
          if(isHash) {
            res.cookie('jid', createRefreshToken(user), {
              httpOnly: true
            })
            return {
              accessToken: createaAccessToken(user),
              user: user.name
            }
          } else {
            return {
              accessToken: '',
              user: ''
            }
          }
        }
      }
    },
    createUser: async (_, { email, password, name, age, job }, __) => {
      if(!email || !password) {
        return {
          success: false,
          message: 'email and password required',
          result: []
        }
      } else {
        const hash = await bcrypt.hash(password, parseInt(process.env.SALT_ROUND))
        if(hash) {
          const result = await Users.create({email, password: hash, name, age, job})
          if(result) {
            return {
              success: true,
              message: 'user created',
              result: [result]
            }
          }
        }
      }
    },
    updateUser: async (_, { id, name, age, job }, __) => {
      let updatedValue = {}
      if(name) updatedValue.name = name
      if(age) updatedValue.age = age
      if(job) updatedValue.job = job
      const result = await Users.findByIdAndUpdate(
        id,
        { $set: updatedValue}, 
        { new: true }
      )
      if(result) {
        return {
          success: true,
          message: 'user is updated',
          result: [result]
        }
      } else {
        return {
          success: false,
          message: 'user is not defined',
          result: []
        }
      }
    },
    deleteUser: async (_, args, __) => {
      const result = await Users.findOneAndDelete({ '_id': args.id })
      if(result) {
        return {
          success: true,
          message: 'user is deleted',
          result: []
        }
      } else {
        return {
          success: false,
          message: 'user is not defined',
          result: []
        }
      }
    }
  }
}