const path = require('path')
const fs = require('fs')
const sharp = require('sharp')

exports.uploadWebp = async (file, pathImage = '') => {
  const { createReadStream, filename } = await file
  const pathDirectory = `../uploads/${pathImage}`
  const formatFilename = filename.trim()
    .toLowerCase()
    .replace(/'/g, '_').replace(/"/g, '_').replace(/ /g, '_').replace(/&/g, '_')

  const splitName = formatFilename.split('.')
  const randomNumber = Math.floor(Math.random() * 999) + 1
  const formatFile = `${splitName[0]}_${randomNumber}.${splitName[1]}`
  const formatWebp = formatFile.replace(/\.(jpg|png|jpeg|gif)$/, `_opt.webp`)

  await new Promise(res => {
    createReadStream()
      .pipe(fs.createWriteStream(path.join(__dirname, pathDirectory, formatFile)))
      .on('close', res)
  })

  await sharp(path.join(__dirname, pathDirectory, formatFile))
    .resize(500, 500, { fit: "inside" })
    .toFile(
      path.join(path.join(__dirname, pathDirectory), formatFile.replace(/\.(jpg|png|jpeg|gif)$/, `_opt$&`))
  )

  await sharp(path.join(__dirname, pathDirectory, formatFile))
    .resize(500, 500, { fit: "inside" })
    .toFile(
      path.join(path.join(__dirname, pathDirectory), formatWebp) 
  )

  fs.unlink(path.join(__dirname, pathDirectory, formatFile), (err) => {
    if(err) console.log(err)
  })

  return `${splitName[0]}_${randomNumber}_opt`
}