const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type Category {
    id: ID,
    name: String
  }

  type Author {
    id: ID,
    name: String
  }

  type Cover {
    filename: String!
    mimetype: String!
    encoding: String!
  }

  type Book {
    id: ID,
    name: String,
    detail: String
    category: Category,
    author: Author,
    slug: String,
    cover: String
  }

  type BookResponse {
    success: Boolean!,
    message: String!,
    result: [Book],
    totalPage: Int
  }

  type Query {
    featureBooks: BookResponse
    books(page: Int!): BookResponse
    book(slug: String!): BookResponse
  }

  type Mutation {
    createBook(name: String!, detail: String!, category: ID!, author: ID!, cover: Upload!): BookResponse
    updateBook(slug: String!, detail: String!, name: String, category: ID!, author: ID!, cover: Upload): BookResponse
    deleteBook(id: ID!): BookResponse
  }
`

module.exports = typeDefs