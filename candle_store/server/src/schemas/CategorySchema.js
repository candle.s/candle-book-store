const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type Category {
    id: ID,
    name: String
  }

  type CategoryResponse {
    success: Boolean
    message: String,
    result: [Category]
  }

  type Query {
    categories: CategoryResponse!
  }

  type Mutation {
    category(id: ID!): CategoryResponse!
    createCategory(name: String!): CategoryResponse!
    editCategory(id: ID!, name: String): CategoryResponse!
    deleteCategory(id: ID!): CategoryResponse!
  }
`

module.exports = typeDefs