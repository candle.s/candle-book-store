const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type Book {
    id: ID,
    name: String
  }

  type Author {
    id: ID
    name: String
    bio: String
    cover: String
    books: [Book]
  }

  type authorsResponse {
    success: Boolean!
    message: String!
    result: [Author]
    totalPage: Int
  }

  type authorResponse {
    success: Boolean!
    message: String!
    result: [Author]
  }

  type Query {
    authors(page: Int): authorsResponse
    author(id: ID): authorResponse
  }

  type Mutation {
    createAuthor(name: String!, bio: String!, cover: Upload!): authorResponse
    updateAuthor(id: ID!, name: String!, bio: String!, cover: Upload): authorResponse
  }
`

module.exports = typeDefs