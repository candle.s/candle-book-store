const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type User {
    id: ID!
    name: String
    age: Int
    job: String
  }

  type Me {
    id: ID!
    email: String
  }

  type Token {
    accessToken: String!,
    user: String
  }

  type checkAuthResponse {
    success: Boolean!
  }

  type UserResponse {
    success: Boolean!
    message: String
    result: [User]!
  }

  type Query {
    users: UserResponse
    me: Me!
  }

  type Mutation {
    revokeRefreshTokenForUser(id: ID!): Boolean!
    checkAuth: checkAuthResponse!
    user(id: ID!): UserResponse!
    login(email: String!, password: String!): Token!
    createUser(email: String!, password: String!, name: String!, age: Int, job: String): UserResponse!
    updateUser(id: ID!, name: String, age: Int, job: String): UserResponse!
    deleteUser(id: ID!): UserResponse!
  }
`

module.exports = typeDefs