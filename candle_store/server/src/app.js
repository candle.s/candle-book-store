const pathDir = require('path')
const express = require('express')
const app = express()
const { ApolloServer } = require('apollo-server-express')
const cors = require('cors')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')
const mongoose = require('mongoose')
const { verify } = require('jsonwebtoken')

const User = require('./models/users')
const { createaAccessToken, createRefreshToken } = require('./auth')

const merge = require('lodash/merge')
const { mergeTypes } = require('merge-graphql-schemas')
const UserSchema = require('./schemas/UserSchema')
const UserResolver = require('./resolvers/UserResolver')
const CategorySchema = require('./schemas/CategorySchema')
const CategoryResolver = require('./resolvers/CategoryResolver')
const BookSchema = require('./schemas/BookSchema')
const BookResolver = require('./resolvers/BookResolver')
const AuthorSchema = require('./schemas/AuthorSchema')
const AuthorResolver = require('./resolvers/AuthorResolver')

const path = '/graphql'

require('dotenv').config()

mongoose.connect(process.env.SERVER_HOST, { useNewUrlParser: true }, (err) => {
  if(err) throw err
  console.log(`connected`)
})

// mongoose.connect(`mongodb://localhost:27017/candle_store`, { useNewUrlParser: true }, (err) => {
//   if(err) throw err
//   console.log(`connected`)
// });

app.use(cors({
  credentials: true,
  origin: ["http://localhost:3000", "http://localhost:5000", "http://localhost:45678"],
  allowedHeaders: ['Content-Type', 'Authorization']
}))
app.use(helmet())
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/uploads', express.static(pathDir.join(__dirname, './uploads')))

app.post('/refresh_token', async (req, res) => {
  const token = req.cookies.jid
  if(!token) {
    return res.status(200).send({ success: false, accessToken: '' })
  }

  let payload = null
  try {
    payload = verify(token, process.env.REFRESH_SECRET)
  } catch(err) {
    console.log(err)
    return res.status(200).send({ success: false, accessToken: '' })
  }

  const user = await User.findOne({ '_id': payload.userId })
  if(!user) {
    return res.status(200).send({ success: false, accessToken: '' })
  }

  if(user.tokenVersion !== payload.tokenVersion) {
    return res.status(200).send({ success: false, accessToken: '' }) 
  }

  res.cookie('jid', createRefreshToken(user), {
    httpOnly: true
  })

  return res.status(200).send({ success: true, accessToken: createaAccessToken(user), user: user.name })
})

app.post('/logout', (req, res) => {
  res.clearCookie('jid')
  res.status(200).json({
    success: true
  })
})

const typeDefs = mergeTypes([UserSchema, CategorySchema, BookSchema, AuthorSchema])
const resolvers = merge(UserResolver, CategoryResolver, BookResolver, AuthorResolver)

const server = new ApolloServer({ 
  typeDefs,
  resolvers,
  context: ({ req, res }) => {
    let payload = null
    try {
      const authorization = req.headers['authorization'] || ''
      const token = authorization.split(' ')[1]
      payload = verify(token, process.env.ACCESS_SECRET)
    } catch (err) {}
    return { req, res, payload }
  },
  playground: {
    settings: {
      "request.credentials": "include"
    }
  }
})
server.applyMiddleware({ app, path, cors: false })

app.listen(3001, () => {
  console.log(`🚀`)  
})