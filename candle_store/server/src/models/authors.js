const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AuthorSchema = new Schema({
  name: {
    type: String,
    require: true,
    unique: true
  },
  bio: {
    type: String,
    required: true
  },
  books: [{
    type: Schema.Types.ObjectId,
    ref: 'book',
    default: []
  }],
  cover: {
    type: String
  }
}, {
  timestamps: true
})

const Author = mongoose.model('author', AuthorSchema)
module.exports = Author