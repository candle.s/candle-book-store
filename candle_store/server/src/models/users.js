const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UsersSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  age: {
    type: Number
  },
  job: {
    type: String
  },
  tokenVersion: {
    type: Number,
    default: 0
  }
}, {
  timestamps: true
})

const Users = mongoose.model('users', UsersSchema)

module.exports = Users