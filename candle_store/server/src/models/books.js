const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BookSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  detail: {
    type: String
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'category',
    required: true
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'author',
    required: true
  },
  cover: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true,
    unique: true
  }
}, {
  timestamps: true
})

const Books = mongoose.model('book', BookSchema)
module.exports = Books