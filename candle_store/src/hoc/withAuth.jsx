import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'

const CHECK_AUTH = gql`
  mutation {
    checkAuth {
      success
    }
  }
`

const withAuth = (WrappedComponent) => {
  return withRouter(({ history }) => {
    const [loading, setLoading] = useState(true)
    const [ checkAuth ] = useMutation(CHECK_AUTH)
    useEffect(() => {
      (async () => {
        const { data } = await checkAuth()
        if(!data.checkAuth.success) {
          return history.push('/login')
        }
      })()
      setLoading(false)
    }, [])

    if(loading) return <div>Loading...</div>
    if(!loading) return <WrappedComponent />

  })
}

export default withAuth