import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Snackbar, SnackbarContent } from '@material-ui/core'
import styled from 'styled-components'
import { useMutation } from '@apollo/react-hooks'
import { connect } from 'react-redux'

import { Formik, Form, Field } from 'formik'
import * as yup from 'yup'

import { LOGIN } from '../gql/login'
import { setAccessToken } from '../utils/accessToken'
import { setUser } from '../actions/user'
import { TextFormField } from '../components/TextFormField'

function LoginComponent({ setUser }) {
  const [snack, setSnack] = useState(false)
  const [ login ] = useMutation(LOGIN)

  const handleClose = () => {
    setSnack(!snack)
  }

  const handleSubmit = async ({ email, password }) => {
    const response = await login({ variables: {email, password}})
    if(!response.data.login.accessToken) {
      return setSnack(true)
    }

    if(response && response.data.login.accessToken) {
      setAccessToken(response.data.login.accessToken)
      setUser(response.data.login.user)
    }
    window.location.href = '/'
  }

  const LoginSchema = yup.object({
    email: yup.string().required().email(),
    password: yup.string().required().min(6)
  })

  return (
    <div className="container">
      <Wrapper className="wrapper">
        <Formik
          enableReinitialize={true}
          initialValues={{
            email: '',
            password: ''
          }}
          validationSchema={LoginSchema}
          onSubmit={values => handleSubmit({...values})}
        >{() => (
          <Form>
            <Field label="Email" name="email" component={TextFormField} variant="outlined" />
            <Field label="Password" name="password" type="password" component={TextFormField} variant="outlined" />
            <div style={{ width: '100%', textAlign: 'center', paddingTop: '20px' }}>
              <button type="submit">Login</button>
            </div>
          </Form>
        )}</Formik>
      </Wrapper>
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        key={{ vertical: 'bottom', horizontal: 'right' }}
        open={snack}
        onClose={handleClose}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
      >
        <SnackbarContent 
          style={{ backgroundColor: '#ffa000', color: '#fff' }}
          message="Login Failed"
        />
      </Snackbar>
    </div>
  )
}

const Wrapper = styled.div`
  width: 30%;
  margin: 100px auto;
  padding: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #BDBDBD;
  border-radius: 4px;

  form {
    width: 100%;
  }

  button {
    padding: 10px 20px;
    border-radius: 4px;
    font-size: 18px;
    background-color: #ff6969;
    color: #fff;
    border: none;
    width: 100%;
    margin-top: 5px;
    cursor: pointer;
  }

  @media (max-width: 414px) {
    width: 80%;
    padding: 20px;
  }
`

const mapDispatchToProps = dispatch => ({
  setUser: user => dispatch(setUser(user))
})

export default connect(null, mapDispatchToProps)(withRouter(LoginComponent))