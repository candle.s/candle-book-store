import React, { lazy, Suspense } from 'react'
import styled from 'styled-components'
import {
  CircularProgress
} from '@material-ui/core'

import { GET_FEATURE_BOOKS } from '../gql/home'
import { useQuery } from '@apollo/react-hooks'

import bookSlide1 from '../assets/book_slide_1.jpg'

import Slider from 'react-slick'
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
}

const RenderBooks = lazy(() => import('../components/RenderBooks'))

function App() {
  const { loading, error, data } = useQuery(GET_FEATURE_BOOKS)

  if (loading) return (
    <div className="circular">
      <CircularProgress size={50} />
    </div>
  )
  if (error) return `Error! ${error}`

  return (
    <div className="container">
      <Slide {...settings}>
        <img src={bookSlide1} alt='slide' />
      </Slide>
      <Wrapper className="wrapper">
        {
          data.featureBooks.result.map(book => (
            <Suspense fallback={<div>Loading....</div>} key={book.id}>
              <RenderBooks {...book} />
            </Suspense>
          ))
        }
      </Wrapper>
    </div>
  )
}

const Slide = styled(Slider)`
  @media (max-width: 414px) {
    img {
      height: 200px;
    }
  }
`

const Wrapper = styled.div`
  width: 100%;
  padding: 50px 0;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 30px;

  @media (max-width: 414px) {
    grid-template-columns: repeat(1, 1fr);
    padding: 20px;
  }
`

export default App
