import React, { useState, useEffect } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { 
  Button, 
  ButtonGroup,
  Dialog,
  DialogActions,
  DialogTitle,
  CircularProgress
} from '@material-ui/core'
import styled from 'styled-components'
import { GET_BOOK, DELETE_BOOK } from '../gql/book'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { connect } from 'react-redux'
import { resetCaches } from 'graphql-tag'

function Book(props) {
  const [deleteOpen, setDeleteOpen] = useState(false)
  const { loading, error, data } = useQuery(GET_BOOK, {
    variables: { slug: props.match.params.slug }
  })
  const [deleteBook, { data: dataDelete }] = useMutation(DELETE_BOOK);

  useEffect(() => {
    window.scrollTo({top: 0})
  })
  
  if (loading) return (
    <div className="circular">
      <CircularProgress size={50} />
    </div>
  )
  if (error) return `Error! ${error}`;

  const { id, name, detail, category, author, cover, slug } = data.book.result[0]

  const handleDelete = async () => {
    const result = await deleteBook({ variables: { id } }, resetCaches())
    if(result.data.deleteBook.success) {
      props.history.push('/')
    }
  }

  const handleClickOpen = () => {
    setDeleteOpen(true)
  }

  const handleClose = () => {
    setDeleteOpen(false)
  }

  return (
    <div className="container">
      {
        !!props.user.user && (
          <UserMenu className="wrapper">
            <ButtonGroup 
              variant="contained"
              color="secondary"
              size="large"
              aria-label="large contained secondary button group">
              <Button>
                <Link to={`/edit_book/${slug}`}>
                  Edit
                </Link>
              </Button>
              <Button onClick={handleClickOpen}>
                Delete
              </Button>
            </ButtonGroup>
          </UserMenu>
        )
      }
      <Wrapper className="wrapper">
        <Cover>
          <picture>
            <source srcSet={`http://localhost:3001/uploads/${cover}.webp`} type="image/webp" />
            <source srcSet={`http://localhost:3001/uploads/${cover}.jpg`} type="image/jpeg" />
            <img src={`http://localhost:3001/uploads/${cover}`} alt={name} />
          </picture>
        </Cover>
        <Detail>
          <DetailHeader>
            <h2>{name}</h2>
            <SubHeader>
              <AuthorAndCategory>
                <h3 style={{ marginRight: '10px', textTransform: 'capitalize' }}>Category: </h3>
                <p>{category.name}</p>
              </AuthorAndCategory>
              <AuthorAndCategory>
                <h3 style={{ marginRight: '10px', textTransform: 'capitalize' }}>Author: </h3>
                <p>{author.name}</p>
              </AuthorAndCategory>
            </SubHeader>
          </DetailHeader>
          <Content>
            <span>{detail}</span>
          </Content>
        </Detail>
      </Wrapper>
      <Dialog
        open={deleteOpen}
        onClose={handleClose}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Are you want to delete item?
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleDelete} color="secondary">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

const Wrapper = styled.div`
  width: 100%;
  padding: 40px 0;
  display: flex;

  @media (max-width: 414px) {
    flex-direction: column;
    padding: 20px;
  }
`

const UserMenu = styled.div`
  text-align: right; 
  padding: 0; 
  margin-top: 50px;

  @media (max-width: 414px) {
    padding-right: 20px;
  }
`

const Cover = styled.div`
  flex: 0 40%;
  text-align: center;

  img {
    width: 100%; 
    height: 600px; 
    max-height: 1000px;
  }

  @media (max-width: 414px) {
    img {
      height: 400px;
    }
  }
`

const Detail = styled.div`
  flex: 1;
  padding-left: 50px;

  span {
    font-size: 20px;
    line-height: 1.6;
  }

  @media (max-width: 414px) {
    padding-top: 10px;
    padding-left: 0;
  }
`

const DetailHeader = styled.div`
  margin-bottom: 30px;
  text-transform: uppercase;

  h2 {
    margin-bottom: 30px;
  }

  h3 {
    color: #373737;
  }

  @media (max-width: 414px) {
    h2 {
      font-size: 20px;
    }
  }
`

const SubHeader = styled.div`
  
`

const AuthorAndCategory = styled.div`
  display: flex; 
  align-items: center; 
  margin-bottom: 10px;
`

const Content = styled.div`
  text-align: justify;
  
  @media (max-width: 414px) {
    text-align: initial;
  }
`

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, null)(withRouter(Book))