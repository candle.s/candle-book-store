import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Button, makeStyles, CircularProgress } from '@material-ui/core'
import styled from 'styled-components'
import { GET_BOOKS } from '../gql/books'
import { useQuery } from '@apollo/react-hooks'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'

import RenderBooks from '../components/RenderBooks'

const useStyles = makeStyles(theme => ({
  button: {},
  input: {
    display: 'none',
  },
}))

function BooksComponent({ user }) {
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const { loading, error, data } = useQuery(GET_BOOKS, 
    { variables: { page },
    fetchPolicy: 'network-only',
    onCompleted: data => data.books.totalPage ? setTotalPage(data.books.totalPage) : null
  })
  const classes = useStyles()

  if (loading) return (
    <div className="circular">
      <CircularProgress size={50} />
    </div>
  )
  if (error) return `Error! ${error}`

  const handlePageClick = data => {
    let selected = data.selected
    setPage(selected)
  }

  return (
    <div className="container">
      <div className="wrapper" style={{ textAlign: 'right', paddingTop: '50px' }}>
        {
          user.user && (
            <Link to='/add_book'>
              <Button variant="contained" color="secondary" className={classes.button}>Add Book</Button> 
            </Link>
          )
        }
      </div>
      <Wrapper className="wrapper">
        {
          data.books.result.map(book => (
            <RenderBooks key={book.id} {...book} />
          ))
        }
      </Wrapper>
      <div className="wrapper">
        <ReactPaginate
          previousLabel={'<'}
          nextLabel={'>'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          forcePage={page}
          pageCount={totalPage}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
      </div>
    </div>
  )
}

const Wrapper = styled.div`
  width: 100%;
  padding: 30px 20px;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 30px;

  @media (max-width: 414px) {
    grid-template-columns: repeat(1, 1fr);
  }
`

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, null)(BooksComponent)
