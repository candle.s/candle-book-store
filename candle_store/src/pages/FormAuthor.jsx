import React from 'react'
import { withRouter } from 'react-router-dom'
import { Formik, Form, Field } from 'formik'
import * as yup from 'yup'
import styled from 'styled-components'
import { 
  Button,
  CircularProgress
} from '@material-ui/core'
import { Query, Mutation } from 'react-apollo'
import { GET_AUTHOR, CREATE_AUTHOR, UPDATE_AUTHOR } from '../gql/formAuthor'

import { TextFormField } from '../components/TextFormField'

class FormBook extends React.Component {
  state = {
    id: '',
    name: '',
    bio: '',
    cover: '',
    coverName: ''
  }

  handleData = data => {
    this.setState({
      id: data[0].id,
      name: data[0].name,
      bio: data[0].bio,
      coverName: data[0].cover
    })
  }
  
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleCover = (setField, file) => {
    setField('cover', file)
    setField('coverName', file.name)
  }

  handleSubmit = async (typeAction, { name, bio, cover }) => {
    try {
      const { type } = this.props
      const id = this.props.match.params.id
      let variable = {}
      let coverObj = {}
      if(type === 'add') {
        variable = {
          name,
          bio,
          cover
        }
      } else {
        if(cover) coverObj = cover
        variable = {
          id,
          name,
          bio,
          cover: coverObj
        }
      }
      const result = await typeAction({
        variables: variable
      })
      if(type === 'add') {
        if(result.data.createAuthor.success) {
          window.location.href = `/authors`
        }
      } else {
        if(result.data.updateAuthor.success) {
          window.location.href = `/authors`
        }
      }
    } catch (err) {
      console.log(err)
    }
  }

  formBookSchema = yup.object({
    name: yup.string()
      .required(),
    bio: yup.string()
      .required()
  })

  render() {
    const { type, match } = this.props
    const { 
      name,
      bio,
      cover,
      coverName
    } = this.state
    return (
      <Mutation mutation={type === 'add' ? CREATE_AUTHOR : UPDATE_AUTHOR}>
        {(actionAuthor, { data }) => {
          return (
            <Query query={GET_AUTHOR} 
              variables={{ id: match.params.id || '' }} 
              onCompleted={dataAuthor => dataAuthor.author.success ? this.handleData(dataAuthor.author.result) : null}
            >
              {({ loading, error, data }) => {
                if(loading) return (
                  <div className="circular">
                    <CircularProgress size={50} />
                  </div>
                )
                if(error) return <p>Error...</p>
                return (
                  <div className="container">
                    <div className="wrapper" style={{ paddingTop: '30px' }}>
                      <h1 style={{ padding: '0 10px' }}>{type === 'add' ? 'Add Author' : 'Edit Author'}</h1>
                      <Formik
                        enableReinitialize={true}
                        validationSchema={this.formBookSchema}
                        initialValues={{
                          name: name,
                          bio: bio,
                          cover: cover,
                          coverName: coverName
                        }}
                        onSubmit={value => this.handleSubmit(actionAuthor, value)}
                        render={({ values, setFieldValue }) => (
                          <Form>
                            <FormGroup>
                              <Field label="Name" name="name" component={TextFormField} />
                              <Field
                                label="Bio" 
                                name="bio" 
                                component={TextFormField}
                                placeholder="bio"
                                multiline={true}
                                rows={12}
                                rowsMax={15}
                                variant="outlined"
                              />
                            </FormGroup>
                            <FormFlex>
                              <FormGroup p style={{ width: 'auto' }}>
                                <input
                                  onChange={({
                                    target: {
                                      validity,
                                      files: [file]
                                    }
                                  }) => validity.valid && this.handleCover(setFieldValue, file)}
                                  accept="image/*"
                                  style={{ display: 'none' }}
                                  name="cover"
                                  id="cover"
                                  type="file"
                                />
                                <label htmlFor="cover">
                                  <Button variant="contained" color="secondary" component="span">
                                    Upload
                                  </Button>
                                </label>
                              </FormGroup>
                              <p>{values.coverName}</p>
                            </FormFlex>
                            <div style={{ width: '100%', textAlign: 'center', paddingTop: '20px' }}>
                              <SubmitForm type="submit">Submit</SubmitForm>
                            </div>
                          </Form>
                        )}
                      />
                    </div>
                  </div>
                )
              }}
            </Query>
          )
        }}
      </Mutation>
    )
  }
}

const FormFlex = styled.div`
  display: flex;
  align-items: center;
`

const FormGroup = styled.div`
  width: 100%;
  padding: 10px;
`

const SubmitForm = styled.button`
  padding: 10px 20px;
  font-size: 18px;
  border: 1px solid #3f51b5;
  background: #3f51b5;
  color: #fff;
  border-radius: 6px;
  cursor: pointer;
`

export default withRouter(FormBook)