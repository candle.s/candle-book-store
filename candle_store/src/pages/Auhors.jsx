import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Button, makeStyles, CircularProgress } from '@material-ui/core'
import styled from 'styled-components'
import { GET_AUTHORS } from '../gql/authors'
import { useQuery } from '@apollo/react-hooks'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'

import RenderAuthors from '../components/RenderAuthors'

const useStyles = makeStyles(theme => ({
  button: {},
  input: {
    display: 'none',
  },
}))

function BooksComponent({ user }) {
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const { loading, error, data } = useQuery(GET_AUTHORS, {
    fetchPolicy: 'network-only',
    onCompleted: data => data.authors.totalPage ? setTotalPage(data.authors.totalPage) : null
  })
  const classes = useStyles()

  if (loading) return (
    <div className="circular">
      <CircularProgress size={50} />
    </div>
  )
  if (error) return `Error! ${error}`

  const handlePageClick = data => {
    let selected = data.selected
    setPage(selected)
  }

  return (
    <div className="container">
      <div className="wrapper" style={{ textAlign: 'right', paddingTop: '50px' }}>
        {
          user.user && (
            <Link to='/add_author'>
              <Button variant="contained" color="secondary" className={classes.button}>Add Author</Button> 
            </Link>
          )
        }
      </div>
      <Wrapper className="wrapper">
        {
          data.authors.result.map(author => (
            <RenderAuthors key={author.id} {...author} />
          ))
        }
      </Wrapper>
      <div className="wrapper">
        <ReactPaginate
          previousLabel={'<'}
          nextLabel={'>'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          forcePage={page}
          pageCount={totalPage}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
      </div>
    </div>
  )
}

const Wrapper = styled.div`
  width: 100%;
  padding: 30px 20px;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 30px;

  @media (max-width: 414px) {
    grid-template-columns: repeat(1, 1fr);
  }
`

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, null)(BooksComponent)
