import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { 
  Button, 
  ButtonGroup,
  CircularProgress
} from '@material-ui/core'
import styled from 'styled-components'
import { GET_AUTHOR } from '../gql/author'
import { useQuery } from '@apollo/react-hooks'
import { connect } from 'react-redux'

function Author(props) {
  const { loading, error, data } = useQuery(GET_AUTHOR, {
    variables: { id: props.match.params.id }
  })
  
  if (loading) return (
    <div className="circular">
      <CircularProgress size={50} />
    </div>
  )
  if (error) return `Error! ${error}`;

  const { id, name, bio, cover } = data.author.result[0]

  return (
    <Container className="container">
      {
        !!props.user.user && (
          <UserMenu className="wrapper">
            <ButtonGroup 
              variant="contained"
              color="secondary"
              size="large"
              aria-label="large contained secondary button group">
              <Button>
                <Link to={`/edit_author/${id}`}>
                  Edit
                </Link>
              </Button>
            </ButtonGroup>
          </UserMenu>
        )
      }
      <Wrapper className="wrapper">
        <Cover>
        <picture>
          <source srcSet={`http://localhost:3001/uploads/authors/${cover}.webp`} type="image/webp" />
          <source srcSet={`http://localhost:3001/uploads/authors/${cover}.jpg`} type="image/jpeg" />
          <img src={`http://localhost:3001/uploads/authors/${cover}`} alt={name} />
        </picture>
        </Cover>
        <Detail>
          <DetailHeader>
            <h2>{name}</h2>
          </DetailHeader>
          <div style={{ textAlign: 'justify' }}>
            <span>{bio}</span>
          </div>
        </Detail>
      </Wrapper>
    </Container>
  )
}

const Container = styled.div`
  @media (max-width: 414px) {
    padding: 20px;
  }
`

const Wrapper = styled.div`
  width: 100%;
  padding: 40px 0;
  display: flex;
  flex-direction: column;
`

const UserMenu = styled.div`
  text-align: right; 
  padding: 0;
  margin-top: 50px;

  @media (max-width: 414px) {
    margin-top: 0;
  }
`

const Cover = styled.div`
  flex: 0 40%;
  text-align: center;

  img {
    width: 300px;
    height: 300px;
    max-height: 300px;
    border-radius: 50%;
  }
`

const Detail = styled.div`
  flex: 1;
  /* padding-left: 50px; */
  margin-top: 50px;

  span {
    font-size: 20px;
    line-height: 1.6;
  }
`

const DetailHeader = styled.div`
  margin-bottom: 30px;
  text-transform: uppercase;

  h2 {
    margin-bottom: 30px;
  }

  h3 {
    color: #373737;
  }
`

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, null)(withRouter(Author))