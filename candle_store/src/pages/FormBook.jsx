import React from 'react'
import { withRouter } from 'react-router-dom'
import { Formik, Form, Field } from 'formik'
import * as yup from 'yup'
import styled from 'styled-components'
import { 
  Button,
  CircularProgress
} from '@material-ui/core'
import { Query, Mutation } from 'react-apollo'
import { GET_AUTHORS, GET_BOOK, GET_CATEGORIES, CREATE_BOOK, UPDATE_BOOK } from '../gql/formBook'

import { TextFormField } from '../components/TextFormField'
import { SelectFormField } from '../components/SelectFormField'

class FormBook extends React.Component {
  state = {
    id: '',
    category: '',
    author: '',
    name: '',
    detail: '',
    cover: '',
    coverName: ''
  }

  handleData = data => {
    this.setState({
      id: data[0].id,
      category: data[0].category.id,
      author: data[0].author.id,
      name: data[0].name,
      detail: data[0].detail,
      coverName: data[0].cover
    })
  }
  
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleCover = (setField, file) => {
    setField('cover', file)
    setField('coverName', file.name)
  }

  handleSubmit = async (typeAction, { name, detail, author, category, cover }) => {
    try {
      const { type } = this.props
      const slug = this.props.match.params.slug
      let variable = {}
      let coverObj = {}
      if(type === 'add') {
        variable = {
          name,
          detail,
          author,
          category,
          cover
        }
      } else {
        if(cover) coverObj = cover
        variable = {
          slug,
          name,
          detail,
          author,
          category,
          cover: coverObj
        }
      }
      const result = await typeAction({
        variables: variable
      })
      if(type === 'add') {
        if(result.data.createBook.success) {
          window.location.href = `/book/${result.data.createBook.result[0].slug}`
        }
      } else {
        if(result.data.updateBook.success) {
          window.location.href = `/book/${result.data.updateBook.result[0].slug}`
        }
      }
    } catch (err) {
      console.log(err)
    }
  }

  formBookSchema = yup.object({
    category: yup.string()
      .required(),
    author: yup.string()
      .required(),
    name: yup.string()
      .required(),
    detail: yup.string()
      .required()
  })

  render() {
    const { type, match } = this.props
    const { 
      category, 
      author, 
      name,
      detail,
      cover,
      coverName
    } = this.state
    return (
      <Query query={GET_CATEGORIES}>
        {({ loading: loadingCategories, error: errorCategories, data: dataCategories }) => {
          return (
            <Query query={GET_AUTHORS}>
              {({ loading: loadingAuthors, error: errorAuthoors, data: dataAuthors }) => {
                return (
                  <Mutation mutation={type === 'add' ? CREATE_BOOK : UPDATE_BOOK}>
                    {(createBook, { data }) => {
                      return (
                        <Query query={GET_BOOK} 
                          variables={{ slug: match.params.slug || '' }} 
                          onCompleted={dataBook => dataBook.book.success ? this.handleData(dataBook.book.result) : null}
                        >
                          {({ loading: loadingBook, error: errorBook, data: dataBook }) => {
                            if(loadingCategories || loadingAuthors || loadingBook) return (
                              <div className="circular">
                                <CircularProgress size={50} />
                              </div>
                            )
                            if(errorCategories || errorAuthoors || errorBook) return <p>Error...</p>
                            return (
                              <div className="container">
                                <div className="wrapper" style={{ paddingTop: '30px' }}>
                                  <h1 style={{ padding: '0 10px' }}>{type === 'add' ? 'Add Book' : 'Edit Book'}</h1>
                                  <Formik
                                    enableReinitialize={true}
                                    validationSchema={this.formBookSchema}
                                    initialValues={{
                                      category: category,
                                      author: author,
                                      name: name,
                                      detail: detail,
                                      cover: cover,
                                      coverName: coverName
                                    }}
                                    onSubmit={value => this.handleSubmit(createBook, value)}
                                    render={({ values, setFieldValue }) => (
                                      <Form>
                                        <FormGroup>
                                          <FormFlex>
                                            <Field
                                              options={dataCategories.categories.result}
                                              label="Category"
                                              name="category"
                                              component={SelectFormField}
                                              style={{ marginRight: '20px' }}
                                            />
                                            <Field
                                              options={dataAuthors.authors.result}
                                              label="Author"
                                              name="author"
                                              component={SelectFormField}
                                            />
                                          </FormFlex>
                                          <Field label="Name" name="name" component={TextFormField} />
                                          <Field 
                                            label="Detail" 
                                            name="detail" 
                                            component={TextFormField}
                                            placeholder="detail"
                                            multiline={true}
                                            rows={10}
                                            rowsMax={10}
                                            variant="outlined"
                                          />
                                        </FormGroup>
                                        <FormFlex>
                                          <FormGroup p style={{ width: 'auto' }}>
                                            <input
                                              onChange={({
                                                target: {
                                                  validity,
                                                  files: [file]
                                                }
                                              }) => validity.valid && this.handleCover(setFieldValue, file)}
                                              accept="image/*"
                                              style={{ display: 'none' }}
                                              name="cover"
                                              id="cover"
                                              type="file"
                                            />
                                            <label htmlFor="cover">
                                              <Button variant="contained" color="secondary" component="span">
                                                Upload
                                              </Button>
                                            </label>
                                          </FormGroup>
                                          <p>{values.coverName}</p>
                                        </FormFlex>
                                        <div style={{ width: '100%', textAlign: 'center', paddingTop: '20px' }}>
                                          <SubmitForm type="submit">Submit</SubmitForm>
                                        </div>
                                      </Form>
                                    )}
                                  />
                                </div>
                              </div>
                            )
                          }}
                        </Query>
                      )
                    }}
                  </Mutation>
                )
              }}
            </Query>
          )
        }}
      </Query>
    )
  }
}

const FormFlex = styled.div`
  display: flex;
  align-items: center;
`

const FormGroup = styled.div`
  width: 100%;
  padding: 10px;
`

const SubmitForm = styled.button`
  padding: 10px 20px;
  font-size: 18px;
  border: 1px solid #3f51b5;
  background: #3f51b5;
  color: #fff;
  border-radius: 6px;
  cursor: pointer;
`

export default withRouter(FormBook)