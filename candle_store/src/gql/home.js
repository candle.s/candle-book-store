import gql from 'graphql-tag'

export const GET_FEATURE_BOOKS = gql`
{
  featureBooks {
    success
    message
    result {
      id
      name
      category {
        id
        name
      }
      cover
      slug
    }
  }
}
`