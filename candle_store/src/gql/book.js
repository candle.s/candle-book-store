import gql from 'graphql-tag'

export const GET_BOOK = gql`
  query book($slug: String!) {
    book(slug: $slug) {
      success
      message
      result {
        id
        name
        detail
        category {
          id
          name
        }
        author {
          id
          name
        }
        cover
        slug
      }
    }
  }
`

export const DELETE_BOOK = gql`
  mutation deleteBook($id: ID!) {
    deleteBook(id: $id) {
      success
      message
    }
  }
`