import gql from 'graphql-tag'

export const GET_AUTHORS = gql`
  {
    authors {
      success
      message
      result {
        id
        name
        bio
        cover
      }
      totalPage
    }
  }
`