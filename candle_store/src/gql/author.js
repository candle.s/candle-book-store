import gql from 'graphql-tag'

export const GET_AUTHOR = gql`
  query authro($id: ID) {
    author(id: $id) {
      success
      message
      result {
        id
        name
        bio
        cover
      }
    }
  }
`