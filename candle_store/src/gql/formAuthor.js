import gql from 'graphql-tag'

export const GET_AUTHOR = gql`
  query author($id: ID) {
    author(id: $id) {
      success
      message
      result {
        id
        name
        bio
        cover
      }
    }
  }
`

export const CREATE_AUTHOR = gql`
  mutation createBook($name: String!, $bio: String!, $cover: Upload!) {
    createAuthor(name: $name, bio: $bio, cover: $cover) {
      success
      message
      result {
        id
        name
      }
    }
  }
`

export const UPDATE_AUTHOR = gql`
  mutation createBook($id: ID!, $name: String!, $bio: String!, $cover: Upload!) {
    updateAuthor(id: $id, name: $name, bio: $bio, cover: $cover) {
      success
      message
      result {
        id
        name
      }
    }
  }
`