import gql from 'graphql-tag'

export const GET_BOOKS = gql`
  query books($page: Int!) {
    books(page: $page) {
      success
      message
      result {
        id
        name
        category {
          id
          name
        }
        slug
        cover
      }
      totalPage
    }
  }
`