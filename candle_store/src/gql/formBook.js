import gql from 'graphql-tag'

export const GET_CATEGORIES = gql`
  {
    categories {
      success
      message
      result {
        id
        name
      }
    }
  }
`

export const GET_AUTHORS = gql`
  {
    authors {
      success
      message
      result {
        id
        name
      }
    }
}
`

export const GET_BOOK = gql`
  query book($slug: String!) {
    book(slug: $slug) {
      success
      message
      result {
        id
        name
        detail
        category {
          id
          name
        }
        author {
          id
          name
        }
        slug
        cover
      }
    }
  }
`

export const CREATE_BOOK = gql`
  mutation createBook($name: String!, $detail: String!, $category: ID!, $author: ID!, $cover: Upload!) {
    createBook(
      name: $name,
      detail: $detail
      category: $category
      author: $author
      cover: $cover
    ) {
      success
      message
      result {
        id
        name
        detail
        category {
          id
          name
        }
        author {
          id
          name
        }
        slug
      }
    }
  }
`

export const UPDATE_BOOK = gql`
  mutation updateBook($slug: String!, $name: String!, $detail: String!, $category: ID!, $author: ID!, $cover: Upload) {
    updateBook(
      slug: $slug
      name: $name
      detail: $detail
      category: $category
      author: $author
      cover: $cover
    ) {
      success
      message
      result {
        id
        name
        detail
        category {
          id
          name
        }
        author {
          id
          name
        }
        slug
      }
    }
  }
`