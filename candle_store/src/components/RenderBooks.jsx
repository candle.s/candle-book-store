import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

function RenderBook({ slug, category, name, cover }) {
  return (
    <Wrapper to={`/book/${slug}`}>
      <div style={{ minHeight: '300px', height: '300px',  maxHeight: '300px' }}>
        <picture>
          <source srcSet={`http://localhost:3001/uploads/${cover}.webp`} type="image/webp" />
          <source srcSet={`http://localhost:3001/uploads/${cover}.jpg`} type="image/jpeg" />
          <img src={`http://localhost:3001/uploads/${cover}`} style={{ width: '100%', height: '100%' }} alt={name} />
        </picture>
      </div>
      <Detail>
        <span>{name}</span>
        <span>{category.name}</span>
      </Detail>
    </Wrapper>
  )
}

const Wrapper = styled(Link)`
  flex: 0 30%;
  border: 1px solid #BDBDBD; 
  transition: all 0.4s ease-out;

  &:hover {
    transform: translateY(-10px);
    box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
  }

  img {
    width: 100%;
  }
`

const Detail = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 20px;

  span:nth-child(1) {
    font-size: 18px;
    line-height: 1.6;
  }

  span:nth-child(2) {
    margin: 10px 0;
  }
`

export default RenderBook