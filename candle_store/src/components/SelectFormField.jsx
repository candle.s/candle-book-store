import { FieldProps, getIn } from 'formik'
import React from 'react'
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from '@material-ui/core'

export const SelectFormField = ({ 
  field, 
  form, 
  label, 
  options,
  style,
  ...props 
}) => {
  const errorText =
    getIn(form.touched, field.name) && getIn(form.errors, field.name)
  return (
    <FormControl fullWidth error={!!errorText} style={{...style}}>
      {label && <InputLabel>{label}</InputLabel>}
      <Select fullWidth {...field} {...props}>
        {options.map(op => (
          <MenuItem key={op.id} value={op.id}>
            {op.name}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>{errorText}</FormHelperText>
    </FormControl>
  )
}