import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

function RenderBook({ id, name, bio, cover }) {
  return (
    <Wrapper to={`/author/${id}`}>
      <AuthorBox>
        <picture>
          <source srcSet={`http://localhost:3001/uploads/authors/${cover}.webp`} type="image/webp" />
          <source srcSet={`http://localhost:3001/uploads/authors/${cover}.jpg`} type="image/jpeg" />
          <img src={`http://localhost:3001/uploads/authors/${cover}`} style={{ width: '200px', height: '200px', borderRadius: '50%' }} alt={name} />
        </picture>
      </AuthorBox>
      <Detail>
        <h3>{name}</h3>
        <span>{bio}</span>
      </Detail>
    </Wrapper>
  )
}

const Wrapper = styled(Link)`
  border: 1px solid #BDBDBD; 
  transition: all 0.4s ease-out;

  &:hover {
    transform: translateY(-10px);
    box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
  }

  img {
    width: 100%;
  }
`

const AuthorBox = styled.div`
  max-height: 300px;
  text-align: center;
  border-bottom: 1px solid #BDBDBD;
  padding: 20px;
`

const Detail = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 20px;
  text-align: center;

  span:nth-child(2) {
    margin: 10px 0;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`

export default RenderBook