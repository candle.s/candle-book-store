import React, { useState, useEffect } from 'react'
import { Link, withRouter } from 'react-router-dom'
import styled from 'styled-components'
import { getAccessToken } from '../utils/accessToken'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
// import { useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'

import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  useMediaQuery,
  useTheme
} from '@material-ui/core'

import { connect } from 'react-redux'
import { logout } from '../actions/user'

const GET_USER = gql`
  query me {
    me {
      id
      email
    }
  }
`;

function NavbarComponent({ user, logout, children }) {
  // const { loading, error, data } = useQuery(GET_USER, { fetchPolicy: 'network-only' })
  const [navActive, setNavActive] = useState(false)
  const [menuActive, setMenuActive] = useState(false)
  const [open, setOpen] = React.useState(false)
  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
  
  useEffect(() => {
    window.addEventListener('scroll', function() {
      if(this.window.pageYOffset >= 50) {
        setNavActive(true)
      } else {
        setNavActive(false)
      }
    })
  })

  const handleMenuActive = () => {
    setMenuActive(active => !active)
  }

  const handleClose = () => {
    setOpen(!open)
  }

  const handleLogout = async () => {
    const response = await fetch('http://localhost:3001/logout', {
      method: 'POST',
      credentials: 'include'
    })
    await response.json()
    setOpen(false)
    window.location.href = '/'
  }

  return (
    <Wrapper>
      <Navbar className={`${navActive ? 'nav_active' : ''}`}>
        <NavbarWrapper className={`wrapper`}>
          <NavTitle>
            <Logo to="/">
              <h2>Candles Store</h2>
            </Logo>
            <BurgerMenu onClick={() => handleMenuActive()}>
              <FontAwesomeIcon icon={faBars} size="lg" />
            </BurgerMenu>
          </NavTitle>
          <Menu className={menuActive ? 'active' : ''}>
            <Link onClick={() => handleMenuActive()} to="/books">Books</Link>
            <Link onClick={() => handleMenuActive()} to="/authors">Author</Link>
            {
              getAccessToken() ? (
                <React.Fragment>
                  <Link to='#' onClick={() => setOpen(true)}>{user ? user : 'annonymous'}</Link>
                </React.Fragment>
              ) : (
                <Link to="/login">Login</Link>
              )
            }
          </Menu>
        </NavbarWrapper>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Are you want to logout?"}</DialogTitle>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleLogout} color="secondary">
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </Navbar>
      {children}
    </Wrapper>
  )
}

const Wrapper = styled.div`

`

const Navbar = styled.div`
  height: 60px;
  display: flex;
  align-items: center;
  width: 100%;
  background-color: #ff6969;
  color: #fff;

  &.nav_active {
    /* position: fixed;
    z-index: 10;
    top: 0; */
  } 

  @media (max-width: 414px) {
    height: auto;
  }
`

const Logo = styled(Link)`
  height: 100%;
  display: flex;
  align-items: center;

  @media (max-width: 414px) {
    h2 {
      font-size: 20px;
    }
  }
`

const NavTitle = styled.div`
  @media (max-width: 414px) {
    padding: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`

const NavbarWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  padding: 0;

  @media (max-width: 414px) {
    display: block;
  }
`

const BurgerMenu = styled.div`
  
`

const Menu = styled.div`
  font-size: 20px;
  margin-left: auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
  text-transform: uppercase;

  a {
    padding: 20px;
    height: 100%;
    font-weight: 500;
    transition: all 0.4s;

    /* &:hover {
      background-color: #ff4b4b;
    } */
  }

  @media (max-width: 414px) {
    flex-direction: column;
    align-items: flex-start;
    max-height: 0;
    transition: all 0.6s;

    &.active {
      max-height: 500px;
    }

    a {
      width: 100%;
      font-size: 18px;
      padding: 10px 20px;
    }
  }
`

const mapStateToProps = state => ({
  user: state.user.user
})

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter((NavbarComponent)))