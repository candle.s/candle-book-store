import { 
  SET_USER,
  LOGOUT 
} from './types'

export const setUser = user => {
  return {
    type: SET_USER,
    payload: user
  }
}

export const logout = _ => {
  return {
    type: LOGOUT
  }
}