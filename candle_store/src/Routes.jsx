import React, { lazy, Suspense } from 'react'
import { CircularProgress } from '@material-ui/core'
import { Route, Switch } from 'react-router-dom'

import Navbar from './components/Navbar'

const Home = lazy(() => import('./pages/Home'))
const FormBook = lazy(() => import('./pages/FormBook'))
const FormAuhtor = lazy(() => import('./pages/FormAuthor'))
const Book = lazy(() => import('./pages/Book'))
const Books = lazy(() => import('./pages/Books'))
const Authors = lazy(() => import('./pages/Auhors'))
const Author = lazy(() => import('./pages/Author'))
const Login = lazy(() => import('./pages/Login'))

function App() {
  return (
    <Navbar>
      <Suspense 
        fallback={
          <div className="circular">
            <CircularProgress size={50} />
          </div>
        }>
        <Switch>
          <Route exact path='/'>
            <Home />
          </Route>
          <Route exact path='/add_book'>
            <FormBook 
              type="add"
            />
          </Route>
          <Route exact path='/add_author'>
            <FormAuhtor 
              type="add"
            />
          </Route>
          <Route exact path='/edit_author/:id'>
            <FormAuhtor 
              type="edit"
            />
          </Route>
          <Route exact path='/edit_book/:slug'>
            <FormBook 
              type="edit"
            />
          </Route>
          <Route exact path='/authors'>
            <Authors />
          </Route>
          <Route exact path='/author/:id'>
            <Author />
          </Route>
          <Route exact path='/books'>
            <Books />
          </Route>
          <Route exact path='/book/:slug'>
            <Book />
          </Route>
          <Route exact path='/login'>
            <Login />
          </Route>
        </Switch>
      </Suspense>
    </Navbar>
  )
}

export default App