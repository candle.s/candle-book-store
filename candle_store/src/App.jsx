import React, { useState, useEffect } from 'react'
import Routes from './Routes'
import { CircularProgress } from '@material-ui/core'
import { setAccessToken } from './utils/accessToken'

import { connect } from 'react-redux'
import { setUser } from './actions/user'

function App(props) {
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    fetch('http://localhost:3001/refresh_token', {
      method: 'POST',
      credentials: 'include'
    }).then(async res => {
      const response = await res.json()
      if(response.success) {
        setAccessToken(response.accessToken)
        props.setUser(response.user)
      }
      setLoading(false)
    })
  }, [])

  if(loading) {
    return (
      <div className="circular">
        <CircularProgress size={50} />
      </div>
    )
  }

  return (
    <React.Fragment>
      <Routes />
    </React.Fragment>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(setUser(user))
  }
}

export default connect(null, mapDispatchToProps)(App)