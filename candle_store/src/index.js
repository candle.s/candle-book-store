import React from 'react'
import { hydrate, render } from 'react-dom'
import './index.css'
// import ApolloClient, { InMemoryCache } from 'apollo-boost'
import { createUploadLink } from 'apollo-upload-client'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import { onError } from 'apollo-link-error'
// import { withClientState } from 'apollo-link-state'
import { TokenRefreshLink } from 'apollo-link-token-refresh'
import { ApolloLink, Observable } from 'apollo-link'
import jwtDecode from 'jwt-decode'
import { Provider } from 'react-redux'
import { ApolloProvider } from '@apollo/react-hooks'
import { BrowserRouter as Router } from 'react-router-dom'
import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import rootReducer from './reducers'

import App from './App.jsx'
import { getAccessToken, setAccessToken } from './utils/accessToken'

const store = createStore(rootReducer, applyMiddleware(logger))

const cache = new InMemoryCache({})

const uploadLink = createUploadLink({ 
  uri: 'http://localhost:3001/graphql', 
  fetch, 
  fetchOptions: { credentials: 'include' } 
})

const requestLink = new ApolloLink((operation, forward) =>
  new Observable(observer => {
    let handle
    Promise.resolve(operation)
      .then(operation => {
        const accessToken = getAccessToken()
        if(accessToken) {
          operation.setContext({
            headers: {
              authorization: `bearer ${accessToken}`
            }
          })
        }
      })
      .then(() => {
        handle = forward(operation).subscribe({
          next: observer.next.bind(observer),
          error: observer.error.bind(observer),
          complete: observer.complete.bind(observer),
        })
      })
      .catch(observer.error.bind(observer))

    return () => {
      if (handle) handle.unsubscribe()
    }
  })
)

const client = new ApolloClient({
  link: ApolloLink.from([
    new TokenRefreshLink({
      accessTokenField: 'accessToken',
      isTokenValidOrUndefined: () => {
        const token = getAccessToken()
        if(!token) {
          return true
        }
        try {
          const { exp } = jwtDecode(token)
          if(Date.now() >= exp * 1000) {
            return false
          } else {
            return true
          }
        } catch(err) {
          return false
        }
      },
      fetchAccessToken: () => {
        return fetch('http://localhost:3001/refresh_token', {
          method: 'POST',
          credentials: 'include'
        })
      },
      handleFetch: accessToken => {
        setAccessToken(accessToken)
      },
      handleError: err => {
        console.warn('Your refresh token is invalid. Try to relogin');
        console.log(err)
      }
    }),
    onError(({ graphQLErrors, networkError }) => {
      console.log(graphQLErrors)
      console.log(networkError)
    }),
    requestLink,
    uploadLink,
    new HttpLink({
      uri: 'http://localhost:3001/graphql',
      credentials: 'include',
    }),
  ]),
  cache
})

const WrapperApp = (
  <ApolloProvider client={client}>
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>
  </ApolloProvider>
)

// ReactDOM.render(WrapperApp, document.getElementById('root'))

const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
  hydrate(WrapperApp, rootElement);
} else {
  render(WrapperApp, rootElement);
}